(function() {
    'use strict';

    angular
        .module('nosqlassignmentApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
