(function () {
    'use strict';

    angular
        .module('nosqlassignmentApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
