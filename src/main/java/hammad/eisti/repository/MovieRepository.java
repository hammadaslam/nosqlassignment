package hammad.eisti.repository;

import hammad.eisti.domain.Movie;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Movie entity.
 */
@SuppressWarnings("unused")
public interface MovieRepository extends MongoRepository<Movie,String> {

}
